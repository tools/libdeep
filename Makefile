#---------------------------------------------------------------------
#
LIBDEEP     = deep


#---------------------------------------------------------------------
#
LIBDEEP_SRC = src/lib$(LIBDEEP).c
LIBDEEP_H   = src/lib$(LIBDEEP).h
LIBDEEP_SO  = src/lib$(LIBDEEP).so
LIBDEEP_OBJ = src/lib$(LIBDEEP).o
LIB_CFLAGS  = -g -Wall -Wno-unused-variable -shared 
CFLAGS = -fPIC

#---------------------------------------------------------------------
#
LIBDEEP_HOME= .
LIBDEEP_DIR = $(LIBDEEP_HOME)/lib
LIBDEEP_INC = $(LIBDEEP_HOME)/include


#---------------------------------------------------------------------
#
lib: $(LIBDEEP_SO)

src/%.o: src/%.c 

$(LIBDEEP_SO): $(LIBDEEP_OBJ)
	$(CC) $(LIB_CFLAGS) -o $@ $<

install: $(LIBDEEP_SO)
	mkdir -p $(PREFIX)/lib && cp $(LIBDEEP_SO) $(PREFIX)/lib
	mkdir -p $(PREFIX)/include && cp $(LIBDEEP_H) $(PREFIX)/include

#---------------------------------------------------------------------
#
clean: 
	rm -f $(LIBDEEP_SO)
	rm -f src/*.o

