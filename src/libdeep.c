//-------------------------------------------------------------------
//
// File: libdeep.c
//
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>      // va_arg, va_start, va_end, ...
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>       // F_SETFl, O_NONBLOCK, etc
#include <netdb.h>       // gethostbyname, h_errno, etc
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/tcp.h> // TCP_NODELAY, etc
#include <netinet/in.h>  // IPPROTO_TCP, etc
#include <arpa/inet.h>   // inet_addr, etc
#include <ctype.h>       // isdigit, isspace, etc

#include "libdeep.h"


// unused attribute
#ifdef __GNUC__
#define ATTRIBUTE_UNUSED __attribute__((unused))
#else
#define ATTRIBUTE_UNUSED
#endif

// ------------------------------------------------------
// SVN/RCS ident
// ------------------------------------------------------

ATTRIBUTE_UNUSED static char SvnDate[]     = "$Date: 2014-03-14 12:56:51 +0100 (Fri, 14 Mar 2014) $";
ATTRIBUTE_UNUSED static char SvnRevision[] = "$Revision: 707 $";
ATTRIBUTE_UNUSED static char SvnAuthor[]   = "$Author: LEMENTEC $";


// ------------------------------------------------------
// Parameter related definitions
// ------------------------------------------------------

typedef struct {
   int   code;
   char *name;
   int   flags;
} parentry_t;
   
#define _RW      0x0000
#define _RO      0x0001

#define DPAR_LIST \
  DPAR(pDEBUG,    "DEBUG",       _RW), \
  DPAR(pERROUTPUT,"ERROROUTPUT", _RW), \
  DPAR(pHOSTNAME, "HOSTNAME",    _RO), \
  DPAR(pPORT,     "PORT",        _RW), \
  DPAR(pTIMEOUT,  "TIMEOUT",     _RW), \
  DPAR(pRECONNECT,"RECONNECT",   _RW), \
  DPAR(pUSECHKSUM,"USECHECKSUM", _RW), \
  DPAR(pICEPAPMD, "ICEPAPMODE",  _RW)

#define DPAR(code, name, flags) code
enum {DPAR_LIST};
#undef DPAR

#define DPAR(code, name, flags) {code, name, flags}
parentry_t par_list[] = { DPAR_LIST };
#undef DPAR

#define DEEPDEV_PAR_ITEMS (sizeof(par_list)/sizeof(parentry_t))

// Library parameters to be managed per connection
typedef struct cpars_s {
  int timeoutms;
  int usechecksum;
  int reconnect;
  int icepapmode;
  int icepapterm;
} cpars_t;

// Global library parameters
struct {
  FILE     *errfd;
  int       debug;
  int       def_ip_port;
  cpars_t   def;          // default 'per connection' parameters
} lib_pars = {
   NULL,       // errfd, this field MUST be initialised to stderr!!
   0,          // debug
   5000,       // def_ip_port
   {
      3000,    // timeoutms
         1,    // usechecksum
         0,    // reconnect
         0,    // icepapmode
         0     // icepapterm
   }
};

// ------------------------------------------------------
// Connection handler related definitions
// ------------------------------------------------------

struct deephandle_s {
  int      signature;

  char    *ip_host;
  int      ip_port;
  int      ip_socket;

  cpars_t  par;

  char    *cmd_buf;
  int      cmd_bufsz;

  char    *prefix;
  int      prefix_len;

  char    *ans_buf;
  int      ans_bufsz;
  char    *ans;

  void    *last_databuf;
};

#define MAX_N_HANDLERS 10           // TODO: remove hardcoded array size
struct deephandle_s  lib_dev_list[MAX_N_HANDLERS];    // device handler list

#define DEEP_HNDL_SIGNATURE 0xdefecada



//---------------------------------------------------
// Debug related
//---------------------------------------------------

#define DEBUG_PREFIX "\tLIBDEEP: "
#define __DPRINTF(n, ...) if (skip_dbg_message(n)); else \
  fprintf(lib_pars.errfd, DEBUG_PREFIX __VA_ARGS__)
#define DEBUGF_1(...)  __DPRINTF(1, __VA_ARGS__)
#define DEBUGF_2(...)  __DPRINTF(2, __VA_ARGS__)
#define DEBUGF_3(...)  __DPRINTF(3, __VA_ARGS__)
#define DEBUGF_4(...)  __DPRINTF(4, __VA_ARGS__)
#define DEBUGF_5(...)  __DPRINTF(5, __VA_ARGS__)

#define IS_DEBUGDUMP() (lib_pars.debug >= 3)
//
#define DEEPDEV_ANS_BUFSZ   2048
#define DEEPDEV_CMD_BUFSZ   1024
#define DEEPDEV_CMD_EOC     '\n'
#define DEEPDEV_BAD_SOCKET -1

#define PFX_ERROR "ERROR"
#define PFX_OK    "OK"

//---------------------------------------------------
// Binary data
//---------------------------------------------------

typedef struct {
   uint32_t  signature;
   uint32_t  size;
   uint32_t  checksum;
} binheader_t;

#define DEEP_SIGNATURE      0xA5A50000
#define DEEP_SIGNATURE_MASK 0xA5A50000
#define DEEP_NOCHECKSUM     0x00000010
#define DEEP_BIGENDIAN      0x00000020
#define DEEP_TYPEMASK       0x0000000F

#define ICEPAP_SIGNATURE    0xA5AA555A
#define ICEPAP_TYPE         BIN_16

/*
#include <netinet/in.h>
#include <Winsock.h>

uint32_t htonl(uint32_t hostlong);
uint16_t htons(uint16_t hostshort);
uint32_t ntohl(uint32_t netlong);
uint16_t ntohs(uint16_t netshort);
*/

int   open_socket (deephandle_t dev);
int   close_socket(deephandle_t dev);

deeperror_t last_error;
char        error_msg[2048];


//-------------------------------------------------------------------
//
//
#define LNCHAR 16
#define GNCHAR  2

void char_dump(unsigned char *buf, int len) {
   int   i;

   fprintf(lib_pars.errfd, DEBUG_PREFIX "dumping %d bytes\n", len );

   for (; len > 0; len -= LNCHAR, buf+= LNCHAR) {
      fprintf(lib_pars.errfd, DEBUG_PREFIX "  ");
      for (i = 0; i < LNCHAR && i < len ; i++)
         fprintf(lib_pars.errfd, "%c", isprint(buf[i])? buf[i] : '.');
      for (; i < LNCHAR ; i++)
         fprintf(lib_pars.errfd, " ");
      fprintf(lib_pars.errfd, " : ");
      for (i = 0; i < LNCHAR && i < len; i++)
	 fprintf(lib_pars.errfd, "%s%02x", !(i % GNCHAR)? " ":"", buf[i]);
      fprintf(lib_pars.errfd, "\n");
   }
}

//-------------------------------------------------------------------
//
//
int skip_dbg_message(int msglvl) {
  // this is needed to be sure that lib_pars.errfd is initialised
  if (lib_pars.errfd == NULL)
     lib_pars.errfd = stderr;

  return(msglvl > lib_pars.debug);
}

//-------------------------------------------------------------------
//
//
deeperror_t lib_error(deeperror_t err, const char *fmt, ...) {
  va_list arg_ptr;
  int     len;

  va_start(arg_ptr, fmt);
  len = vsprintf(error_msg, fmt, arg_ptr);
  error_msg[len] = 0;
  va_end(arg_ptr);

  DEBUGF_1("ERROR: %s\n", error_msg);

  return(last_error = err);
}

//-------------------------------------------------------------------
//
//
deeperror_t deepdev_error(char **errmsg) {
  *errmsg = error_msg;
  return(last_error);
}


//-------------------------------------------------------------------
//
//
int is_valid_handle(deephandle_t dev) {
   int offset = ((void *)dev) - (void *)lib_dev_list;

   if (offset < 0 || (offset % sizeof(struct deephandle_s)) ||
             (offset / sizeof(struct deephandle_s)) >= MAX_N_HANDLERS)
      return(0);
   else
      return(dev->signature == DEEP_HNDL_SIGNATURE);
}

//-------------------------------------------------------------------
//
//
deephandle_t new_handle(void) {
   int i;

   for (i = 0; i < MAX_N_HANDLERS; i++) {
      if (lib_dev_list[i].signature != DEEP_HNDL_SIGNATURE) {
         lib_dev_list[i].par     = lib_pars.def;
         lib_dev_list[i].ip_host = NULL;
         lib_dev_list[i].ip_port = lib_pars.def_ip_port;
         return(&lib_dev_list[i]);
      }
   }
   return(BAD_HANDLE);
}

//-------------------------------------------------------------------
//
//
int set_connection_timeout(int ip_socket, int timeoutms) {
   struct timeval timeout;

   timeout.tv_sec  = timeoutms / 1000;
   timeout.tv_usec = (timeoutms % 1000) * 1000;
   if (setsockopt(ip_socket, SOL_SOCKET, SO_RCVTIMEO,\
              (const void *)&timeout, sizeof(timeout)) < 0) {
      lib_error(DEEPDEV_COMMERR, "cannot set timeout: %s\n", strerror(errno));
      return(0);
   } else
      return(1);
}

//-------------------------------------------------------------------
//
//
deephandle_t deepdev_open(char *hostname) {
   deephandle_t dev;

   DEBUGF_2("deepdev_open(\"%s\")\n",hostname);

   if ((dev = new_handle()) == BAD_HANDLE) {
      lib_error(DEEPDEV_ERR, "fail to allocate handle");
      return(BAD_HANDLE);
   }
   if ((dev->ip_host = (char *)malloc(strlen(hostname)+1)) == NULL) {
      lib_error(DEEPDEV_ERR, "fail to allocate name buffer");
      return(BAD_HANDLE);
   }
   strcpy(dev->ip_host, hostname);

   dev->signature = DEEP_HNDL_SIGNATURE;

   if (!open_socket(dev)) {
      if(!dev->par.reconnect) {
          deepdev_close(dev);
          return(BAD_HANDLE);
      }
      DEBUGF_2("RECONNECT: connection failed but continue anyway\n");
   }
   DEBUGF_3("returning from open_socket()\n");

   if ((dev->ans_buf = (char *)malloc(DEEPDEV_ANS_BUFSZ)) == NULL) {
      lib_error(DEEPDEV_ERR, "fail to allocate answer buffer");
      deepdev_close(dev);
      return(BAD_HANDLE);
   }
   dev->ans_bufsz = DEEPDEV_ANS_BUFSZ;
   DEBUGF_3("end of malloc()\n");
 
   if ((dev->cmd_buf = (char *)malloc(DEEPDEV_CMD_BUFSZ)) == NULL) {
      lib_error(DEEPDEV_COMMERR, "fail to allocate command buffer");
      deepdev_close(dev);
      return(BAD_HANDLE);
   }
   dev->cmd_bufsz = DEEPDEV_CMD_BUFSZ;
   DEBUGF_3("end of malloc()\n");

   // Normal end
   DEBUGF_2("end of deepdev_open()\n");
   return(dev);
}


//-------------------------------------------------------------------
//
//
deeperror_t deepdev_close(deephandle_t dev) {
   DEBUGF_2("deepdev_close()\n");

   if (!is_valid_handle(dev))
      return(lib_error(DEEPDEV_ERR, "bad connection handle"));

   close_socket(dev);

   if (dev->ans_buf) free(dev->ans_buf);
   if (dev->cmd_buf) free(dev->cmd_buf);
   if (dev->ip_host) free(dev->ip_host);
   dev->signature = 0;

   // Normal end
   return(DEEPDEV_OK);
}



//-------------------------------------------------------------------
//
//
parentry_t *parse_parameter(char *name) {
   int i;

   DEBUGF_2("parse_parameter(\"%s\")\n", name);

   // Valid parameter?
   for(i = 0; i < DEEPDEV_PAR_ITEMS; i++) {
      if(!strcasecmp(par_list[i].name, name))
         return(&par_list[i]);
   }
   lib_error(DEEPDEV_ERR, " unknown parameter");
   return(NULL);
}

//-------------------------------------------------------------------
//
//
deeperror_t deepdev_setparam(deephandle_t dev, char *name, deeppar_t valptr) {
   parentry_t *par;
   int         intval = DDPARtoINT(valptr);
  // char     *strval = DDPARtoSTR(valptr);
   FILE       *outerr = DDPARtoFILE(valptr);

   cpars_t    *ptable;

   if (dev == LIB_DEFAULTS)
      ptable = &lib_pars.def;
   else if (is_valid_handle(dev))
      ptable = &dev->par;
   else
      return(lib_error(DEEPDEV_ERR, "bad connection handle"));

   par = parse_parameter(name);
   if (!par)
      return(last_error);
   else if(par->flags & _RO)
      return(lib_error(DEEPDEV_ERR, "read only parameter"));

   switch(par->code) {
      case pDEBUG:
         if ((intval < 0) || (intval > 6))
            return(lib_error(DEEPDEV_ERR, "invalid parameter value"));
         else
            lib_pars.debug = intval;
         break;
 
      case pERROUTPUT:
         lib_pars.errfd = outerr;
         break;

      case pPORT:
         if (dev != LIB_DEFAULTS)
            return(lib_error(DEEPDEV_ERR, 
               "Cannot change port of open connection"));
         else
            lib_pars.def_ip_port = intval;
         break;

      case pTIMEOUT:
         if (dev != LIB_DEFAULTS) {
            if (!set_connection_timeout(dev->ip_socket, intval))
               return(last_error);
         }
         ptable->timeoutms = intval;
         break;

      case pRECONNECT:
         if (intval < 0)
            return(lib_error(DEEPDEV_ERR, "invalid reconnect value"));
         else
            ptable->reconnect = intval;
         break;

      case pUSECHKSUM:
         ptable->usechecksum = (intval != 0);
         break;

      case pICEPAPMD:
         ptable->icepapmode = (intval != 0);
         break;

      default:
         return(lib_error(DEEPDEV_ERR, "parameter not implemented"));
   }
   // Normal end
   DEBUGF_3("changing \"%s\" parameter\n", par->name);
   return(DEEPDEV_OK);
}


//-------------------------------------------------------------------
//
//
deeperror_t deepdev_getparam(deephandle_t dev, char *name, deeppar_t *valptr) {
   parentry_t *par;
   cpars_t    *ptable;

   if (dev == LIB_DEFAULTS)
      ptable = &lib_pars.def;
   else if (is_valid_handle(dev))
      ptable = &dev->par;
   else
      return(lib_error(DEEPDEV_ERR, "bad connection handle"));

   par = parse_parameter(name);
   if (!par)
      return(DEEPDEV_ERR);

   switch(par->code) {
      case pDEBUG:
         *valptr = DDPAR(lib_pars.debug);
         break;

      case pERROUTPUT:
         *valptr = DDPAR(lib_pars.errfd);
         break;

      case pHOSTNAME:
         if (dev == LIB_DEFAULTS)
            return(lib_error(DEEPDEV_ERR, "No default hostname"));
         else
            *valptr = (deeppar_t) dev->ip_host; // Cannot use DDPAR() here
         break;

      case pPORT:
         if (dev == LIB_DEFAULTS)
            *valptr = DDPAR(lib_pars.def_ip_port);
         else
            *valptr = DDPAR(dev->ip_port);
         break;

      case pTIMEOUT:
         *valptr = DDPAR(ptable->timeoutms);
         break;

      case pRECONNECT:
         *valptr = DDPAR(ptable->reconnect);
         break;

      case pUSECHKSUM:
         *valptr = DDPAR(ptable->usechecksum);
         break;

      case pICEPAPMD:
         *valptr = DDPAR(ptable->icepapmode);
         break;

      default:
         return(lib_error(DEEPDEV_ERR, "parameter not implemented"));
   }
   // Normal end
   return(DEEPDEV_OK);
}


//-------------------------------------------------------------------
//
//

int send_to_device(deephandle_t dev, void *buff, int size) {
   int ret;

   DEBUGF_3("send_to_device(): %d bytes\n", size);
   if (IS_DEBUGDUMP()) char_dump(buff, size);
 
   ret = write(dev->ip_socket, buff, size);
   if (ret != size) {
      char *syserrmsg = (ret == -1)? strerror(errno) : "";
      lib_error(DEEPDEV_COMMERR, "write failed: %s", syserrmsg);
      return(0);
   } else
      return(1);
}

//-------------------------------------------------------------------
//
//
int send_cmd(deephandle_t dev) {
   // Send the command to the device
   return(send_to_device(dev, dev->cmd_buf, strlen(dev->cmd_buf)));
}

//-------------------------------------------------------------------
//
//
int read_one_line(deephandle_t dev, char *answer) {
   char *line = answer;

   while(1) {
      if (read(dev->ip_socket, line, 1) == -1) {
         if(errno != EINTR) {
            lib_error(DEEPDEV_COMMERR, "read failed: %s", strerror(errno));
            return(-(line - answer));
         } else {
            DEBUGF_1("WARNING: %s\n", strerror(errno));
         }
      } else {
         char c;

         if (iscntrl(c = *line)) {
            if (*line == DEEPDEV_CMD_EOC) {
	       DEBUGF_3("new line: [%.*s]\n", (int)(line - answer), answer);
               return(line - answer);
            }
         } else
            line++;
      }
   }
}

//-------------------------------------------------------------------
//
//
int socket_read(int fd, void *buf, int nbytes, int maxbytes) {
   int nret;
   int nread;

   if (nbytes > maxbytes) {
      lib_error(DEEPDEV_ERR, "internal function call error");
      return(0);
   } else if (nbytes <= 0)
      nbytes = 0;
   else 
      maxbytes = nbytes;

   for (nread = 0; nread < maxbytes; ) {
       nret = read(fd, buf + nread, maxbytes - nread);
       if (nret >= 0) {
          nread += nret;
          if (nbytes == 0)
             break;
       } else if (errno == EINTR) {
          DEBUGF_1("WARNING: socket_read(): %d : %s\n", 
             errno, strerror(errno));
          continue;
       } else {
          lib_error(DEEPDEV_COMMERR, "read failed: %d : %s", 
             errno, strerror(errno));
          return(nret);
       }
   }
   DEBUGF_3("socket_read(): %d bytes succesfully read\n", nread);
   if (IS_DEBUGDUMP()) char_dump(buf, nread);
   return(nread);
}

//-------------------------------------------------------------------
//
//
int get_answer(deephandle_t dev, char **answ) {
   char *answer = NULL;
   char  line_n = 0;
   char *ptr = dev->ans_buf;
   int   len;

   // Get answer from device
/*
   if (dev->par.icepapmode) {   // No terminator handling
      ret_len = socket_read(dev->ip_socket, dev->ans_buf, 0, dev->ans_bufsz);
      answer = dev->ans_buf;
      if (ret_len < 0)
         return(0);
      ptr = dev->ans_buf + ret_len;
      if (ret_len == dev->ans_bufsz)
         ptr--;
      *ptr-- = 0;
      while (iscntrl(*ptr))
         *ptr-- = 0;

   } else
*/
   
   while(1) {
      len = read_one_line(dev, ptr);

      if (len <= 0) {
         return(0);
      } else {
         if (!line_n++) {
            //check prefix
            if (strncasecmp(ptr, dev->prefix, dev->prefix_len) != 0) {
               lib_error(DEEPDEV_COMMERR, "bad answer prefix: %.*s", 
                  len, ptr);
               return(0);
            } else {
               ptr += dev->prefix_len;
               len -= dev->prefix_len;
               while(isspace(*ptr) && *ptr != DEEPDEV_CMD_EOC) {
                  ptr++;
                  len--;
               }
               answer = ptr;
               if (ptr[0] == '$' && ptr[1] == DEEPDEV_CMD_EOC)
                  continue;
               else {
                  ptr[len] = 0;
                  break;
               }
            }
         } else if (ptr[0] == '$' && ptr[1] == DEEPDEV_CMD_EOC) {
            if (line_n == 2)
               *ptr = 0;
            else
               *--ptr = 0;
            break;
         } else {
            ptr += len + 1;
         }
      }
   }

   // Check if error answer from device
   if (strncmp(answer, PFX_ERROR, sizeof(PFX_ERROR) - 1) == 0) {
      // Remove "ERROR" prefix
      answer += sizeof(PFX_ERROR);
      while (isspace(*answer))
         answer++;
      lib_error(DEEPDEV_ANSERR, answer);
      if (answ) *answ = answer;
      return(0);

   } else { // Normal end
      if (answ)
         *answ = answer;
      else
         DEBUGF_3("No answer pointer, answer string discarded\n");
      return(1);
   }
}


//-------------------------------------------------------------------
//
//
int open_socket(deephandle_t dev) {
   struct sockaddr_in   ip_address;
   struct hostent      *ip_hp;
   int                  ip_socket;
   int                  ip_status;
   char                *ip_host;
   int                  ip_port;
   int                  ip_on;



   // By default, connection failed
   dev->ip_socket = DEEPDEV_BAD_SOCKET;

   // Get host information
   ip_port = dev->ip_port;
   ip_host = dev->ip_host;
   DEBUGF_3("connecting to \"%s:%d\"\n",ip_host,ip_port);
   if ((ip_hp = gethostbyname(ip_host)) == NULL) {
      lib_error(DEEPDEV_ERR, "unknown hostname: %s\n", hstrerror(h_errno));
      return(0);
   }
   DEBUGF_3("end of gethostbyname()!!\n");


   // Create a socket
   ip_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (ip_socket < 0) {
      lib_error(DEEPDEV_COMMERR, "cannot create a socket: %s", strerror(errno));
      return(0);
   }
   DEBUGF_3("end of socket()\n");

   // Set it to non-blocking
   ip_on = 1;
   ip_status = setsockopt(ip_socket, IPPROTO_TCP, TCP_NODELAY,\
           (const char *)&ip_on, sizeof(int));
   if (ip_status < 0) {
      lib_error(DEEPDEV_COMMERR, "cannot set non-blocking socket: %s", 
           strerror(errno));
      return(0);
   }
   DEBUGF_3("end of setsockopt()\n");

   if (!set_connection_timeout(ip_socket, lib_pars.def.timeoutms))
      return(last_error);

   DEBUGF_3("end of set_connection_timeout()\n");
   // Attempt to initiate a connection
   ip_address.sin_family = AF_INET;
   ip_address.sin_port   = htons(ip_port);
   bcopy(ip_hp->h_addr, &ip_address.sin_addr, ip_hp->h_length);

   ip_status = connect(ip_socket, (struct sockaddr *)&ip_address, 
      sizeof(struct sockaddr));
   if (ip_status < 0) {
      lib_error(DEEPDEV_COMMERR, "cannot connect to ip node: %s", 
         strerror(errno));
      return(0);
   }
   DEBUGF_3("end of connect()\n");

   // Normal end
   dev->ip_socket = ip_socket;
   return(1);
}


//-------------------------------------------------------------------
//
//
int close_socket(deephandle_t dev) {

   DEBUGF_2("closing connection to %s:%d\n", dev->ip_host, dev->ip_port);

   if (dev->ip_socket != DEEPDEV_BAD_SOCKET) {
      shutdown(dev->ip_socket, SHUT_RDWR);
      close(dev->ip_socket); 
   }
   // Normal end
   dev->ip_socket = DEEPDEV_BAD_SOCKET;
   return(1);
}



//-------------------------------------------------------------------
//
// Command Syntax: [#][<address>:]<keyword> [param1 [param2] etc][\r]\n
// Keyword Syntax: [?|*|?*]<string>
//
int parse_cmd(char *buf, int *rflags) {
   char *firstchar = buf;
   char *ptr;
   int   flags;
   int   prefix_len;
   int   addr_len;

   // Check if acknowledge command type
   if (*firstchar == '#') {
      flags = DEEPDEV_FLG_ACK;
      firstchar++;
   } else 
      flags = 0;

   // Skip any spurious control character/terminator
   ptr = buf + strlen(buf) - 1;
   while(isspace(*ptr) || iscntrl(*ptr))
      ptr--;
   // And then add a single command terminator
   *++ptr = DEEPDEV_CMD_EOC;
   *++ptr = 0;

   prefix_len = ptr - firstchar - 1;
   if ((ptr = strchr(firstchar, ' ')) != NULL)
      prefix_len = ptr - firstchar;

   if ((ptr = strchr(firstchar, ':')) && (ptr - firstchar) < prefix_len) {
      addr_len = ptr - firstchar;
      if (addr_len == 0) {
         flags |= DEEPDEV_FLG_BCAST;
         prefix_len -= 1;
      }
      firstchar = ptr + 1;
   }

   // Guess command type
   if(*firstchar == '?') { flags |= DEEPDEV_FLG_QUERY; firstchar++; }
   if(*firstchar == '*') { flags |= DEEPDEV_FLG_BIN;   firstchar++; }
   DEBUGF_3("command type  : %s%s%s%s%s\n",    \
      (flags & DEEPDEV_FLG_ADDR ?"ADDR " :""), \
      (flags & DEEPDEV_FLG_BCAST?"BCAST "  :""), \
      (flags & DEEPDEV_FLG_ACK  ?"ACK "  :""), \
      (flags & DEEPDEV_FLG_QUERY?"QUERY ":""), \
      (flags & DEEPDEV_FLG_BIN  ?"BIN "  :""));

   // Normal end
   *rflags = flags;
   return(prefix_len);
}

//-------------------------------------------------------------------
//
//
int build_cmd(char *buf, int *flags, char *addr, char *cmd, int ack) {
   if (!cmd) {
      lib_error(DEEPDEV_ERR, "missing command");
      return(0);
   } else {
      char c;
      // Remove any leading space and check acknowledge character
      while(isspace(c = *cmd)) cmd++;
      if (c == '#') {
         c = *++cmd;
         ack = 1;
      }
      if (c != ':' && c != '?' && c != '*' && c != '_' && !isalnum(*cmd)) {
         lib_error(DEEPDEV_ERR, "bad formed command: \"%s\"", cmd);
         return(0);
      }
   }
   if (addr) {
      // Remove any leading space and check basic address format
      // Note that addr here may be an empty string (BROADCAST_ADDR)
      while(isspace(*addr)) addr++;
      if (*addr && *addr != '_' && !isalnum(*addr) && strstr(addr, ":")) {
         lib_error(DEEPDEV_ERR, "bad formed address: \"%s\"", addr);
         return(0);
      }
      sprintf(buf, "%s%s:%s", ack? "#":"", addr, cmd);
   } else
      sprintf(buf, "%s%s", ack? "#":"", cmd);

   return(parse_cmd(buf, flags));
}

//-------------------------------------------------------------------
//
//
deeperror_t deepdev_parsecommand(int *flags, char *addr, char *cmd, int ack) {
   char buf[2048];

   DEBUGF_2("deepdev_parsecommand()\n");
   if (!build_cmd(buf, flags, addr, cmd, ack)) {
      *flags = 0;
      return(DEEPDEV_ERR);
   } else
      return(DEEPDEV_OK);
}

//-------------------------------------------------------------------
//
//
int check_binary_data(deephandle_t dev, deepbindata_t *dbin) {
   if (!dbin) {
      lib_error(DEEPDEV_ERR, "Missing valid binary block pointer");
      return(0);
/*
   } else if (dev->par.icepapmode && dbin->datatype != ICEPAP_TYPE) {
      lib_error(DEEPDEV_ERR, "Wrong data type for IcePAP devices");
      return(0);
*/
   }  else {
      // TODO: check consistency
   }
   return(1);
}

//-------------------------------------------------------------------
//
//
uint32_t calc_checksum(uint32_t nvalues, int type, void *buff) {
   register uint32_t checksum = 0;

   switch(type) {
      case BIN_8: {
            uint8_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;

      case BIN_16: {
            uint16_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;

      case BIN_32: {
            uint32_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;

      case BIN_64: {
            uint64_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;
   }
   return(checksum);
}


//-------------------------------------------------------------------
//
//
int send_binary_data(deephandle_t dev, deepbindata_t *dbin) {
   binheader_t header;
   int         skip_checksum;
   int         datatype;
   int         datasize;
   int         nbytes;
   uint32_t    signature;
   uint32_t    checksum;
  
   DEBUGF_2("send_binary_data()\n");
  
   nbytes = dbin->datasize * dbin->datatype;
   if (dev->par.icepapmode) {
      DEBUGF_2("  - Using IcePAP mode\n");
      if (nbytes % 2)
         nbytes++;
      datatype = dbin->datatype;
      datasize = nbytes / BIN_16;

      skip_checksum = 0;
      signature = ICEPAP_SIGNATURE;
      
   } else {
      datatype = dbin->datatype;
      datasize = dbin->datasize;

      skip_checksum = 1;
      if (__BYTE_ORDER == __LITTLE_ENDIAN)
         signature = DEEP_SIGNATURE | DEEP_NOCHECKSUM;
      else
         signature = DEEP_SIGNATURE | DEEP_BIGENDIAN | DEEP_NOCHECKSUM;
      signature |= (uint8_t)datatype;
   }

   checksum = skip_checksum ?  0 :
                 calc_checksum(dbin->datasize, datatype, dbin->databuf);

   DEBUGF_2("  - Datatype: %d byte(s)/value\n", datatype);
   DEBUGF_2("  - Datasize: %d values, %d bytes\n", datasize, nbytes);
   DEBUGF_2("  - Checksum field: 0x%08X - skip check: %s\n",
                              checksum, skip_checksum? "YES":"NO");
   header.signature = TO_DEEP_ENDIAN(signature);
   header.size      = TO_DEEP_ENDIAN(datasize);
   header.checksum  = TO_DEEP_ENDIAN(checksum);

   if (!send_to_device(dev, &header, sizeof(header)))
      return(0);

   return(send_to_device(dev, dbin->databuf, nbytes));
}

//-------------------------------------------------------------------
//
//
int get_binary_data(deephandle_t dev, deepbindata_t *dbin) {
   binheader_t header;
   uint32_t    signature;
   uint32_t    checksum;
   int         skip_checksum;
   ATTRIBUTE_UNUSED int swap_bytes;
   int         bad_signature;
   int         nbytes;

   DEBUGF_2("get_binary_data()\n");
   if (!socket_read(dev->ip_socket, &header, sizeof(header), sizeof(header))) {
      lib_error(DEEPDEV_COMMERR, "binary header read failed: %s", 
         strerror(errno));
      return(0);
   }
   signature = TO_DEEP_ENDIAN(header.signature);
   DEBUGF_2("  Signature field: 0x%08X\n", signature);
   if (dev->par.icepapmode) {
      DEBUGF_2("  - Using IcePAP mode\n");
      bad_signature = (signature != ICEPAP_SIGNATURE);
      skip_checksum = 0;         // checksum is mandatory in IcePAP
      swap_bytes = (__BYTE_ORDER != __ICEPAP_ORDER);
      dbin->datatype = BIN_16;   // 
   }  else {
      DEBUGF_2("  - Using standard DEEP mode\n");
      bad_signature = ((signature & DEEP_SIGNATURE_MASK) != DEEP_SIGNATURE);
      skip_checksum = (signature & DEEP_NOCHECKSUM);
      if (signature & DEEP_BIGENDIAN)
         swap_bytes = (__BYTE_ORDER != __BIG_ENDIAN);
      else
         swap_bytes = (__BYTE_ORDER != __LITTLE_ENDIAN);
      dbin->datatype = (signature & DEEP_TYPEMASK);
   }
   if (bad_signature) {
      lib_error(DEEPDEV_COMMERR, "bad signature: 0x%08X", signature);
      return(0);
   }
   dbin->datasize = TO_DEEP_ENDIAN(header.size);
   checksum = TO_DEEP_ENDIAN(header.checksum);
   nbytes = dbin->datasize * dbin->datatype;
   DEBUGF_2("  - Datatype: %d byte(s)/value\n", dbin->datatype);
   DEBUGF_2("  - Datasize: %d values, %d bytes\n", dbin->datasize, nbytes);
   DEBUGF_2("  - Checksum field: 0x%08X - skip check: %s\n",
                              checksum, skip_checksum? "YES":"NO");

   if (dbin->databuf && dbin->bufsize < nbytes) {
      if (dbin->databuf == dev->last_databuf) {
         DEBUGF_3("current data buffer too small, reallocating\n");
         free(dbin->databuf);
         dbin->databuf = NULL;
      } else {
         lib_error(DEEPDEV_COMMERR, "data buffer is too small");
         // TODO: empty socket
         return(0);
      }
   }
   if (!dbin->databuf) {
      DEBUGF_3("allocating new data buffer\n");
      dbin->bufsize = nbytes;
      if (!(dbin->databuf = dev->last_databuf = malloc(nbytes))) {
         lib_error(DEEPDEV_COMMERR, "memory allocation error");
         // TODO: empty socket
         return(0);
      }
   }

   if (dev->par.icepapmode) {
      dbin->datatype  = BIN_8;    
      dbin->datasize *= 2;

      DEBUGF_2("  - Forcing datatype to BIN_8 in IcePAP mode\n");
      DEBUGF_2("  - Datatype: %d byte(s)/value\n", dbin->datatype);
      DEBUGF_2("  - Datasize: %d values, %d bytes\n", dbin->datasize, nbytes);
   }

   if (socket_read(dev->ip_socket, dbin->databuf, nbytes, nbytes) < 0)
      return(0);
   else 
      return(1);
}


//-------------------------------------------------------------------
//
//
deeperror_t  deepdev_command(deephandle_t dev, char *addr, char *cmd,
                                int ack, char **answ, deepbindata_t *dbin) {
   int flags;

   DEBUGF_2("deepdev_command()\n");

   // Minimum checks
   if (!is_valid_handle(dev) || 
      (dev->prefix_len = build_cmd(dev->cmd_buf, &flags, addr, cmd, ack)) <= 0)
      return(DEEPDEV_ERR);

   // Handle automatic reconnection
   if(dev->ip_socket == DEEPDEV_BAD_SOCKET) {
      DEBUGF_2("RECONNECT: try to restore connection\n");
      if (!open_socket(dev)) {
         DEBUGF_2("RECONNECT: failed\n");
         return(DEEPDEV_COMMERR);
      }
   }

   dev->prefix = dev->cmd_buf;
   while(*dev->prefix == '#' || *dev->prefix == ':')
      dev->prefix++;
   DEBUGF_3("command: %.*s[%.*s]%s",
      (int)(dev->prefix - dev->cmd_buf), dev->cmd_buf,
      dev->prefix_len, dev->prefix,
      dev->prefix + dev->prefix_len);



   if (IS_QUERY(flags) && !answ) {
      lib_error(DEEPDEV_ERR, "missing pointer to device answer");
      return(last_error);

   } else if (IS_BINARY_COMMAND(flags) && !check_binary_data(dev, dbin))
      return(last_error);

   if (!send_cmd(dev))
      return(last_error);
   if (IS_BINARY_COMMAND(flags) && !send_binary_data(dev, dbin))
      return(last_error);
   if (RETURNS_ANSWER(flags)) {
      if (!get_answer(dev, answ))
        return(last_error);
   } else if (answ)
      *answ = NULL;

   if (IS_BINARY_QUERY(flags) && !get_binary_data(dev, dbin))
      return(last_error);
   else
      return(DEEPDEV_OK);
}

